﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Data_to_Text_File
{
    public partial class FriendFile : Form
    {
        public FriendFile()
        {
            InitializeComponent();
        }

        private void WriteNameButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Declare a StreamWriter variable
                StreamWriter outputFile;

                //Open the Friend.txt file for appending
                //and get a StreamWriter object
                outputFile = File.AppendText("Friend.txt");

                //Create a file and get a StreamWriter object
                outputFile = File.CreateText("Friend.txt");

                //Write the friend's name to the file
                outputFile.WriteLine(NameTextBox.Text);

                //Close the file
                outputFile.Close();

                //Let the user know the nae is written
                MessageBox.Show("The name was written.");

                //Clear the nameTextBox control
                NameTextBox.Text = "";

                //Give the focus to the nameTextBox control
                NameTextBox.Focus();
            }
            catch(Exception ex)
            {
                //Display an error message
                MessageBox.Show(ex.Message);
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            //Close the form
            this.Close();
        }
    }
}
